package main

import (
	"fmt"
	"math"
)

func sayGreetings(n string) {
	fmt.Printf("Hello %v \n", n)
}
func sayBye(n string) {
	fmt.Printf("GoodBye %v \n", n)
}

func cycleNames(n []string, f func(string)) {
	for _, v := range n {
		f(v)
	}
}

func circleArea(r float64) float64 {
	return math.Pi * r * r
}

func main() {
	// sayGreetings("Mehedi")
	// sayBye("Mehedi")
	cycleNames([]string{"Mehedi", "Sara", "Muaaz"}, sayGreetings)
	cycleNames([]string{"Mehedi", "Sara", "Muaaz"}, sayBye)
	a1 := circleArea(2.5)
	a2 := circleArea(6)
	fmt.Println(a1, a2)
}
