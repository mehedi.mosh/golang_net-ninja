package main

import (
	"fmt"
)

func main() {
	name := "Mehedi"
	age2 := "30"
	age := 30
	fmt.Print("Hello") // does not add new line
	fmt.Print("World! \n")
	fmt.Print("New Line! \n") // \n adds a new line

	fmt.Println("Hello dudes") // automatically adds new line
	fmt.Println("How its going")
	fmt.Println("My name is", name, "and my age is", age)

	//Printf (Formatted String)

	fmt.Printf("My name is %v and my age is %v \n", name, age)  //%v takes the provided vars order wise
	fmt.Printf("My name is %q and my age is %q \n", name, age2) //%q ads quotes the provided vars order wise
	fmt.Printf("Age is of type %T \n", age2)                    //%T displays the type of the provided vars
	fmt.Printf("Your score is %f \n", 225.5)                    //%T displays the type of the provided vars
	fmt.Printf("Your score is %.1f \n", 225.5)                  //%T displays the type of the provided vars

	//Spritf (Saved formatted String)
	var str = fmt.Sprintf("My name is %v and my age is %v \n", name, age) //%v takes the provided vars order wise
	fmt.Println("The saved string is:", str)
}
