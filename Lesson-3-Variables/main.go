package main

import (
	"fmt"
)

func main() {
	//variables

	// string and numbers
	var firstName string = "Mehedi"
	var lastName = "Mosh" //also a string var declaration
	var nickName string
	nickName = "Abdullah" //declaring the var not not assigning a value
	age := 30
	var favNumber int = 17 // shorthand for declaring var, can not use outside a function
	fmt.Println(firstName, lastName, nickName, age, favNumber)

	//bits and memory and floats
	var numOne int8 = 25 // each int has its own range of values, see docs
	var numTwo uint = 30 // uint does not allow to have negative numbers
	var scoreOne float32 = 25.96
	var scoreTwo float64 = 256546341314654654.96

	fmt.Println(numOne, numTwo, scoreOne, scoreTwo)
}
