package main

import (
	"fmt"
)

func main() {
	age := 25
	if age < 30 {
		fmt.Println("Age is less than 40")
	} else if age < 40 {

		fmt.Println("Age is Forty")
	} else {
		fmt.Println("Age is not less than 45")

	}

	names := []string{"Mehedi", "Sara", "Riyadh", "Muaaz", "Etu"}

	for index, value := range names {
		if index == 1 {
			fmt.Println("Continueing at pos", index)
			continue //get back to continue with the loop
		}
		if index > 2 {
			fmt.Println("Breaking at pos", index)
			break // breaks the loop completely

		}

		fmt.Printf("The value at the pos %v is %v \n", index, value)
	}
}
