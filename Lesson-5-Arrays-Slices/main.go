package main

import (
	"fmt"
)

func main() {
	// var ages [3]int = [3]int{20, 25, 30}
	var ages = [3]int{20, 25, 35}

	names := [4]string{"mehedi", "sara", "etu", "riyadh"}
	names[2] = "nisha"
	fmt.Println(ages, len(ages))
	fmt.Println(names, len(names))

	// slices
	var scores = []int{25, 35, 45}
	scores = append(scores, 85)
	fmt.Println(scores)

	//slice ranges
	namesOne := names[1:3]  // includes the 1 position not the 3 and returns new slice
	namesTwo := names[3:]   // includes the 3 till the end
	namesThree := names[:4] // includes from the begginning till 4, not including the 4
	fmt.Println(namesOne, namesTwo, namesThree)

	namesOne = append(namesOne, "sara")
	fmt.Println(namesOne)
}
