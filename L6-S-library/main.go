package main

import (
	"fmt"
	"sort"
)

func main() {
	// greetings := "Hello There everyone"
	// fmt.Println(strings.Contains(greetings, "hello"))
	// fmt.Println(strings.ReplaceAll(greetings, "Hello", "Hi"))
	// fmt.Println(strings.ToUpper(greetings))
	// fmt.Println(strings.Index(greetings, "ll"))
	// fmt.Println(strings.Split(greetings, " "))

	ages := []int{25, 45, 5, 20, 65, 40, 20, 9}
	sort.Ints(ages)
	fmt.Println(ages)

	index := sort.SearchInts(ages, 40)
	fmt.Println(index)

	names := []string{"Mehedi", "Sara", "Riyadh", "Muaaz", "Etu"}
	sort.Strings(names)
	fmt.Println(names)
	fmt.Println(sort.SearchStrings(names, "Muaaz"))
}
